from setuptools import setup, find_packages

with open("requirements.txt") as f:
	install_requires = f.read().strip().split("\n")

# get version from __version__ variable in aar_enterprise/__init__.py
from aar_enterprise import __version__ as version

setup(
	name="aar_enterprise",
	version=version,
	description="AAR",
	author="Anju Prasannan",
	author_email="anju.prasannan@nestorbird.com",
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
